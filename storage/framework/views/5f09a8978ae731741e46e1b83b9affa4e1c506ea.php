<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
  <div class="site-wrap">
  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

   <div class="row pt-4 text-center">
        <div class="col-12">
          <h2 class="text-white mb-4" id="header">Protocolos</h2>
        </div>
  </div>
  
  <?php $__currentLoopData = $protocolos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $protocolo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <main class="main-content">
    <div class="container-fluid photos">
      <div class="row justify-content-center">
        <div class="col-md-6 pt-4">
          <h4 class="text-white mb-4" data-aos="fade-up"><?php echo e($protocolo->titulo); ?> en el día del <?php echo e($protocolo->fecha); ?></h4>
          <h6 class="text-white"><?php echo e($protocolo->descripcion); ?><h6>
            <?php if(Auth::check() && Auth::user()->rol==1): ?>
                <a href="editar-protocolo/<?php echo e($protocolo->id); ?>"  style="margin:15px; float:left;" class="btn btn-light">Editar</a>
                <form action="<?php echo e(route('eliminar-protocolo')); ?>" method="post" style="float:left;">
                    <?php echo csrf_field(); ?> 
                    <div hidden>
                        <input name="id" value="<?php echo e($protocolo->id); ?>">
                    </div>
                    <input type="submit" style="margin:15px;" onclick="return confirm('¿Seguro que quieres eliminarlo?')" class="btn btn-danger" value="Eliminar" name="submit">
                </form>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </main>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  </body>
</html>
<?php /**PATH /var/www/trollers/html/releases/33/resources/views/general/Protocolos/protocolos.blade.php ENDPATH**/ ?>