<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
  <div class="site-wrap">
  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

   <div class="row pt-4 text-center">
        <div class="col-12">
          <h2 class="text-white mb-4" id="header">Boletín Oficial del Estado</h2>
        </div>
  </div>

  <?php $__currentLoopData = $articulos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $articulo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <?php if($articulo->mode == 0): ?>
  <main class="main-content">
    <div class="container-fluid photos">
      <div class="row justify-content-center">
        <div class="col-md-10 pt-4">
          <h2 class="text-white mb-4" data-aos="fade-up"><?php echo e($articulo->titulo); ?></h2>
          <h4 class="text-white mb-4" data-aos="fade-up">Decretado por el <?php echo e($articulo->ministerio); ?></h4>
          <h4 class="text-white mb-4" data-aos="fade-up">Publicado el día <?php echo e($articulo->fecha); ?></h4>
          <h6 class="text-white"><?php echo $articulo->descripcion ?></h6>
            <?php if($articulo->link): ?>
               <?php if(Auth::check()): ?>
                <a href="<?php echo e($articulo->link); ?>" style="color: blue;" >Enlace para ver el documento: <?php echo e($articulo->titulo); ?></a><br><br>
              <?php else: ?>
                <a style="color: red;" >Si eres miembro inicia sesión para visualizar</a><br><br>
              <?php endif; ?>
            <?php endif; ?>
            <?php if(Auth::check() && Auth::user()->rol==1): ?>
                <a href="editar-boe/<?php echo e($articulo->id); ?>"  style="margin:15px; float:left;" class="btn btn-light">Editar</a>
                <form action="<?php echo e(route('eliminar-boe')); ?>" method="post" style="float:left;">
                    <?php echo csrf_field(); ?> 
                    <div hidden>
                        <input name="id" value="<?php echo e($articulo->id); ?>">
                    </div>
                    <input type="submit" onclick="return confirm('¿Seguro que quieres eliminarlo?')" style="margin:15px;" class="btn btn-danger" value="Eliminar" name="submit">
                </form>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </main>
  <?php endif; ?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  </body>
</html>
<?php /**PATH /home/vagrant/code/trollers/resources/views/general/BOE/boe.blade.php ENDPATH**/ ?>