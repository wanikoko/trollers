<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
  <div class="site-wrap">
  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.Terminal.terminal.estilos', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  <main class="main-content"> 
        <div id="terminal-window">
        <span id="cursor"></span>
        <p>
            <span>ACCEDIENDO A SERVICIOS DE INTELIGENCIA</span>
        </p>
        <p>
            <span>SERVICIO DE INFRAESTRUCTURAS DIGITALES © TROLLERS 2021</span>
        </p>
        <p>
            <span>AUTENTICANDO DESDE EL <?php echo e(strtoupper(Auth::user()->cargo)); ?></span>
        </p>
        <p>
            <span>UBICACIÓN: <?php echo e(strtoupper($ciudad)); ?></span>
        </p>
        <!-- <p>
            <span class="animate"></span><span class="animate">***********</span>
            <br>
            <span class="animate"></span><span class="animate">COMPROBANDO CONEXIÓN</span>
        </p>
        <p>
            <span class="animate"></span><span class="animate">***********</span>
            <br>
            <span class="animate"></span><span class="animate">BIENVENIDO <?php echo e(strtoupper(Auth::user()->name)); ?></span>
        </p> -->
        <p>
            <span class="animate">> </span><span autofocus contenteditable="true" id="inputcmd"></span>
        </p>
        <p><span id="output"></span></p>
        </div>
        <div class="scanlines"></div> 
  </main>

  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php echo $__env->make('general.Terminal.terminal.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  </body>
</html>

<?php /**PATH /Users/carlosrobles/Servidor/Trollers/resources/views/general/Terminal/terminal/index.blade.php ENDPATH**/ ?>