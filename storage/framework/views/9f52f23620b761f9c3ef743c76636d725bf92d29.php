<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
  <div class="site-wrap">
  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.Stories.estilos', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  <main class="main-content">
    <div class="container-fluid photos">
      <div class="row justify-content-center">
        <div class="col-md-6 pt-4">
        <div class="wrapper" data-pos="0">
      <header>
        <div class="slider__dots">
        </div>

        <h3>Featured</h3>
        <div class="extra">
          <i class="zmdi zmdi-replay"></i>
          <i class="zmdi zmdi-more-vert"></i>
        </div>
      </header>
      <div class="slider">
        <div class="slider__slides">

        </div>
      </div>
         
    </div>
        </div>
      </div>
    </div>
  </main>
  <?php echo $__env->make('general.Stories.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  </body>
</html>
<?php /**PATH /home/vagrant/code/trollers/resources/views/general/Stories/index.blade.php ENDPATH**/ ?>