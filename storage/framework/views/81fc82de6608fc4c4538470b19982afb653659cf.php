<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
  <div class="site-wrap">
  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

   <div class="row pt-4 text-center">
        <div class="col-12">
          <h2 class="text-white mb-4" id="header"><?php echo e(trans('messages.gab')); ?></h2>
        </div>
  </div>
  
  <main class="main-content">
    <div class="container-fluid photos">
      <div class="row justify-content-center">

        <div class="col-md-6 pt-4">

          <div class="row">
              <?php $__currentLoopData = $usuarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <div class="col-md-12" style="margin-bottom:40px;" data-aos="fade-up">
                    <div class="d-flex blog-entry align-items-start">
                        <div class="col-md-12">
                          <div class="mr-5 img-wrap"><a href="#"><img src="images/perfiles/<?php echo e($usuario->imagen); ?>" alt="Image" width="260px" height="260px"></a></div><br>
                          <h2 class="mt-0 mb-2"><a href="#"><?php echo e($usuario->name); ?></a></h2>
                          <h2 class="mt-0 mb-4"><a href="#"><?php echo e($usuario->cargo); ?></a></h2>
                          <h6 class="text-white"><?php echo e($usuario->descripcion); ?></h6>
                        </div>
                    </div>
                </div><br>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
          </div>
        </div>
      </div>
      <?php echo $__env->make('general.Footer.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      
    </main>

  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  </body>
</html>
<?php /**PATH /var/www/trollers/html/releases/33/resources/views/general/Formacion/formacion.blade.php ENDPATH**/ ?>