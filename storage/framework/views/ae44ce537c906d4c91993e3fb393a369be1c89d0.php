
<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>

  <div class="site-wrap">
  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  <?php if(Auth::user()->pago==0): ?>
    <p style="background-color:red; color:white; padding:20px; border-radius: 20px; margin:15px; width:80%;">Aún no has realizado ningún pago este año</p>
  <?php else: ?>
    <p style="background-color:#10DE5A; color:white; padding:20px; border-radius: 20px; margin:15px; width:80%;">Tienes todos los pagos en orden :)</p>
  <?php endif; ?>

  <main class="main-content" style=" border-radius:10px; background-color:white; text-align:center;">
    <div class="container">
      <div class="row">
      <table class="table">
        <thead> 
            <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Estado</th>
            <th scope="col">Fecha</th>
            <?php if(Auth::user()->id==1): ?>
            <th scope="col">Acciones</th>
            <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $usuarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                <th scope="row"><?php echo e($usuario->id); ?></th>
                <td><?php echo e($usuario->name); ?></td>
                <td>
                <?php if($usuario->pago==1): ?>
                    <p style="color:green">Pagado</p>
                    <?php else: ?>
                    <p style="color:red">No Pagado</p>
                <?php endif; ?>        
                </td>
                <td>
                <?php if($usuario->fecha_pago): ?>
                <?php echo e($usuario->fecha_pago); ?>

                <?php else: ?> 
                -
                <?php endif; ?>
                </td>
                    <td>
                        <form action="<?php echo e(route('pagar')); ?>" method="post">
                            <?php echo csrf_field(); ?> 
                            <div class="form-group" hidden>
                                <input name="id" value="<?php echo e($usuario->id); ?>">
                            </div>
                            <div class="form-group">
                            <?php if(Auth::user()->id==1 && $usuario->pago==0): ?>
                                <input type="submit" style="border-radius: 10px; background-color: #83DFA4;" value="<?php echo e(trans('messages.2p')); ?>" name="submit"></div>
                            <?php endif; ?>
                        </form>
                        <form action="<?php echo e(route('renovar')); ?>" method="post">
                            <?php echo csrf_field(); ?> 
                            <div class="form-group" hidden>
                                <input name="id" value="<?php echo e($usuario->id); ?>">
                            </div>
                            <div class="form-group">
                            <?php if(Auth::user()->id==1 && $usuario->pago==1): ?>
                                <input type="submit" style="border-radius: 10px; background-color: #83D3DF;" value="<?php echo e(trans('messages.re')); ?>" name="submit"></div>
                            <?php endif; ?>
                        </form>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
    </div>
    </div>
    <?php echo $__env->make('general.Footer.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </main>
      
  

  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  </body>
</html>
<?php /**PATH /var/www/trollers/html/releases/33/resources/views/general/usuarios/pagos.blade.php ENDPATH**/ ?>