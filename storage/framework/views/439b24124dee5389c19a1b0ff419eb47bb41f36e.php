<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>


  <div class="site-wrap">

  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  <main class="main-content">
    <div class="container-fluid photos">
      <div class="row justify-content-center">

        <div class="col-md-8 pt-4">

          <div class="row mb-5" data-aos="fade-up">
            <div class="col-12">
              <h2 class="text-white mb-4 text-center"><?php echo e(trans('messages.opr')); ?></h2>
            </div>
          </div>
          <div class="row">
              <?php $__currentLoopData = $proyectos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $proyecto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-12" data-aos="fade-up">
                  <div class="d-flex blog-entry align-items-start">
                    <div>
                      <?php if($proyecto->id==1): ?>
                        <h2 class="mt-0 mb-3"><a href="#"><?php echo e($proyecto->titulo); ?></a> ⭐</h2>
                      <?php else: ?>
                        <h2 class="mt-0 mb-3"><a href="#"><?php echo e($proyecto->titulo); ?></a></h2>
                      <?php endif; ?>
                      <div class="meta mb-3"><?php echo e(trans('messages.place')); ?>: <?php echo e($proyecto->lugar); ?></div>
                      <p><p><?php echo e($proyecto->descripcion); ?></p></p>
                      <?php if($proyecto->nivel==10): ?>
                      <p><p><?php echo e(trans('messages.lvl')); ?> <a style="color: #A10000"> -<?php echo e($proyecto->nivel); ?>- </a> <?php echo e(trans('messages.dg')); ?></p></p>
                      <?php elseif($proyecto->nivel==11): ?>
                      <p><p><?php echo e(trans('messages.lvl')); ?> <a style="color: #603388"> -<?php echo e($proyecto->nivel); ?>- </a> <?php echo e(trans('messages.dg')); ?></p></p>
                      <?php elseif($proyecto->nivel==9): ?>
                      <p><p><?php echo e(trans('messages.lvl')); ?> <a style="color: #E74C3C"> -<?php echo e($proyecto->nivel); ?>- </a> <?php echo e(trans('messages.dg')); ?></p></p>
                      <?php elseif($proyecto->nivel==8): ?>
                      <p><p><?php echo e(trans('messages.lvl')); ?> <a style="color: #EC7063"> -<?php echo e($proyecto->nivel); ?>- </a> <?php echo e(trans('messages.dg')); ?></p></p>
                      <?php elseif($proyecto->nivel==7): ?>
                      <p><p><?php echo e(trans('messages.lvl')); ?> <a style="color: #E67E22"> -<?php echo e($proyecto->nivel); ?>- </a> <?php echo e(trans('messages.dg')); ?></p></p>
                      <?php elseif($proyecto->nivel==6): ?>
                      <p><p><?php echo e(trans('messages.lvl')); ?> <a style="color: #EB984E"> -<?php echo e($proyecto->nivel); ?>- </a> <?php echo e(trans('messages.dg')); ?></p></p>
                      <?php elseif($proyecto->nivel==5): ?>
                      <p><p><?php echo e(trans('messages.lvl')); ?> <a style="color: #EBD14E"> -<?php echo e($proyecto->nivel); ?>- </a> <?php echo e(trans('messages.dg')); ?></p></p>
                      <?php elseif($proyecto->nivel==4): ?>
                      <p><p><?php echo e(trans('messages.lvl')); ?> <a style="color: #BCE846"> -<?php echo e($proyecto->nivel); ?>- </a> <?php echo e(trans('messages.dg')); ?></p></p>
                      <?php elseif($proyecto->nivel==3): ?>
                      <p><p><?php echo e(trans('messages.lvl')); ?> <a style="color: #ABEBC6"> -<?php echo e($proyecto->nivel); ?>- </a> <?php echo e(trans('messages.dg')); ?></p></p>
                      <?php elseif($proyecto->nivel==2): ?>
                      <p><p><?php echo e(trans('messages.lvl')); ?> <a style="color: #58D68D"> -<?php echo e($proyecto->nivel); ?>- </a> <?php echo e(trans('messages.dg')); ?></p></p>
                      <?php else: ?>
                      <p><p><?php echo e(trans('messages.lvl')); ?> <a style="color: #77FF33"> -<?php echo e($proyecto->nivel); ?>- </a><?php echo e(trans('messages.dg')); ?> </p></p>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
                <?php if(Auth::check() && Auth::user()->rol==1): ?>
                  <form action="<?php echo e(route('eliminar-proyecto')); ?>" method="post" style="float:left;">
                      <?php echo csrf_field(); ?> 
                      <div hidden>
                          <input name="id" value="<?php echo e($proyecto->id); ?>">
                      </div>
                      <input type="submit" style="margin-bottom:15px; margin-left:8px;" onclick="return confirm('¿Seguro que quieres eliminarlo?')" class="btn btn-danger" value="Eliminar" name="submit">
                  </form>
                <?php endif; ?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
          </div>
        </div>
      </div>

      <?php echo $__env->make('general.Footer.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      
    </main>

  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

  </body>
</html>
<?php /**PATH /var/www/trollers/html/releases/33/resources/views/general/Proyectos/proyectos.blade.php ENDPATH**/ ?>