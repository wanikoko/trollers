<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
  <div class="site-wrap">
  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  
  <main class="main-content"> 
    <div class="container-fluid photos">
      <div class="row justify-content-center">
        <div class="col-md-6 pt-4">
          <h4 class="text-white mb-4" data-aos="fade-up">Selecciona un evento a eliminar</h4>
          <form action="<?php echo e(route('eliminar-evento')); ?>" method="post" enctype="multipart/form-data">
            <?php echo csrf_field(); ?> 
                <div class="form-group">
                    <select name="id" id="id" style="background-color: white; color:black; border-radius: 10px; padding: 15px; width: 100%;">
                        <?php $__currentLoopData = $fechas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fecha): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($fecha->id); ?>">
                            <?php if($fecha->tipo == 1): ?>
                                Cumple: 
                            <?php elseif($fecha->tipo == 2): ?>
                                Comida / Cena: 
                            <?php else: ?>
                                Fiesta: 
                            <?php endif; ?>                 
                            <?php echo e($fecha->titulo); ?> el <?php echo e($fecha->fecha); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>             
                </div>
                <div class="form-group">
                    <input type="submit" style="border-radius: 10px; background-color: #FFC686;" value="Eliminar evento" name="submit">
                    <a href="./calendario" style="margin:10px;" class="btn btn-light"><?php echo e(trans('messages.volver')); ?></a>
                </div>
          </form>
        </div>
      </div>
    </div>
  </main>

  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </body>
</html>

<style>
.form-control{
    border-bottom: 2px solid white;
}
</style>

<script>$('#inputDate').datepicker({
});
</script><?php /**PATH /home/vagrant/code/trollers/resources/views/general/Calendar/delEvent.blade.php ENDPATH**/ ?>