<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
  <div class="site-wrap">
  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  
  <main class="main-content"> 
    <div class="container-fluid photos">
      <div class="row justify-content-center">
        <div class="col-md-6 pt-4">
          <h4 class="text-white mb-4" data-aos="fade-up">Responde una notificación</h4>
          <h5 class="text-white mb-4" data-aos="fade-up">Redacción del <?php echo e(App\User::where("id",Auth::user()->id)->get()[0]->cargo); ?></h5>
          <h5 class="text-white mb-4" data-aos="fade-up">Excelentísimo señor <?php echo e(App\User::where("id",Auth::user()->id)->get()[0]->name); ?></h5>
          <form action="<?php echo e(route('responder-notificacion')); ?>" method="post" enctype="multipart/form-data">
            <?php echo csrf_field(); ?> 
                <div class="form-group">
                    <input name="cabecera" style="background-color: white; color:black; border-radius: 10px; padding: 15px; width: 100%;" placeholder="Cabecera" maxlength="250" required>                
                </div>
                <div class="form-group">
                    <textarea id="textArea" class="form-control" name="mensaje" style="background-color: white; border-radius: 10px; padding: 15px; width: 100%;" placeholder="Mensaje" rows="15" maxlength="4000"></textarea>
                </div>
                <div class="form-group">
                    <select name="id_destino" id="id_destino" style="background-color: white; color:black; border-radius: 10px; padding: 15px; width: 100%;">
                        <option value="0">Envio: Grupal</option>
                        <?php $__currentLoopData = $usuarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($usuario->id == $notificacion[0]->id_origen): ?>
                            <option value="<?php echo e($usuario->id); ?>" selected>Destinatario: <?php echo e($usuario->cargo); ?></option>
                            <?php else: ?>
                            <option value="<?php echo e($usuario->id); ?>">Destinatario: <?php echo e($usuario->cargo); ?></option>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>             
                </div>
                <div class="form-group">
                    <label class="text-white mb-4" style="margin-top:10px;"><?php echo e(trans('messages.i')); ?> (Opcional):</label>
                    <input type="file" name="fileToUpload" id="fileToUpload">
                </div>
                <div class="form-group">
                    <input type="submit" style="border-radius: 10px; background-color: #FFC686;" value="Responder notificación" name="submit">
                    <a href="./ver-notificacion-<?php echo e($notificacion[0]->id); ?>" style="margin:10px;" class="btn btn-light"><?php echo e(trans('messages.volver')); ?></a>
                </div>
          </form>
        </div>
      </div>
    </div>
  </main>

  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </body>
</html>

<style>
.form-control{
    border-bottom: 2px solid white;
}
</style>

<?php echo $__env->make('general.Editor.editor', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/trollers/resources/views/general/Notificaciones/responder.blade.php ENDPATH**/ ?>