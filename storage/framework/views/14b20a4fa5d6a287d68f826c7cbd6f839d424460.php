<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-7315335954466454" crossorigin="anonymous"></script>
<body>
  <div class="site-wrap" >

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

    <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <main class="main-content">
      <div class="container-fluid photos">
        <div class="row pt-4 mb-5 text-center">
          <div class="col-12">
            <h2 class="text-white mb-4" id="header"><?php echo e(trans('messages.o')); ?></h2>
            <?php /* <!-- @include('general.Home.countdown') --> */ ?> 
            <?php if($alerta->alternative): ?>
              <?php echo $__env->make('general.Home.info.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php endif; ?>
          </div>
        </div>


        <div class="row align-items-stretch">

          <?php $__currentLoopData = $imagenes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $imagen): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="col-6 col-md-6 col-lg-6 " data-aos="fade-up">
            <a href="images/fotos/<?php echo e($imagen->name); ?>" class="d-block photo-item" data-fancybox="gallery">
              <img src="images/fotos/<?php echo e($imagen->name); ?>" alt="Image" class="img-fluid">
              <div class="photo-text-more">
                <span class="icon icon-expand"></span>
              </div>
            </a>
            <?php if($edit): ?>
            <form action="<?php echo e(route('eliminar')); ?>" method="post">
              <?php echo csrf_field(); ?>
              <div class="form-group" hidden>
                <input name="id" value="<?php echo e($imagen->id); ?>">
              </div>
              <div class="form-group" hidden>
                <input name="name" value="<?php echo e($imagen->name); ?>">
              </div>
              <div class="form-group">
                <input type="submit" style="margin-bottom:15px;" onclick="return confirm('¿Seguro que quieres eliminar esta foto?')" class="btn btn-danger" value="<?php echo e(trans('messages.del')); ?>" name="submit">
              </div>
            </form>
            <?php endif; ?>
          </div>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

          <iframe src="https://open.spotify.com/embed/playlist/2KVu7MJkFM5uZ0oaAlOB3V" style="margin:15px;" width="100%" height="500px" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
          <iframe src="https://discordapp.com/widget?id=538027285580873728&theme=dark" style="margin:15px;" width="100%" height="500px" allowtransparency="true" frameborder="0"></iframe>

        </div>

        <?php echo $__env->make('general.Footer.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

      </div>
    </main>

    <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</body>

</html>
<?php /**PATH /Users/carlosrobles/Servidor/Trollers/resources/views/general/Home/home.blade.php ENDPATH**/ ?>