<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
  <div class="site-wrap">
  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.Notificaciones.estilos', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  
  <main class="main-content">
    <div class="container-fluid photos">
      <div class="row justify-content-center">
        <div class="col-md-6 pt-4">
        <div class="row pt-4 mb-3 text-center">
        </div>
          <a class='button' style="color: white;" href='/notificaciones'>
                Volver
           </a>
           <?php if($notificacion[0]->id_origen != Auth::user()->id): ?>
           <a class='button' style="color: white; float:right;" href='responder-notificacion-<?php echo e($notificacion[0]->id); ?>'>
                Responder
           </a>
           <?php endif; ?>
           <br><br><br>
        <main class="page">
    <section class="page__container">
            <h1>
            <?php echo e($notificacion[0]->cabecera); ?>

              </h1><br>
                <p>
                  Escrita por el <?php echo e(App\User::where("id",$notificacion[0]->id_origen)->get()[0]->cargo); ?>

                </p>
                <p>
                  Dirigida al <?php echo e(App\User::where("id",$notificacion[0]->id_destino)->get()[0]->cargo); ?>

                </p>
                <?php if($notificacion[0]->grupal): ?>
                <p>
                  Notificacion enviada a todos los Ministerios, el dia <?php echo e($notificacion[0]->fecha); ?>

                </p>
                <?php else: ?>
                <p>
                  Fecha de envio: <?php echo e($notificacion[0]->fecha); ?>

                </p>
                <?php endif; ?>
                <?php if($notificacion[0]->id_destino != Auth::user()->id): ?>
                <?php if(!$notificacion[0]->leido): ?>
                <p><b>Notificación no leida por el destinatario</b></p>
                <?php else: ?>
                <p>Notificación leida por el destinatario</p>
                <?php endif; ?>
                <?php endif; ?>
                <main>
                  <table>
                    <thead>
                      <tr>
                        <th>
                          Mensaje
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td data-title='Mensaje'>
                            <?php echo $notificacion[0]->mensaje ?>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </main>
                <?php if($notificacion[0]->imagen): ?>
                <main>
                  <table>
                    <thead>
                      <tr>
                        <th>
                          Imagen adjunta
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td data-title='Imagen'>
                            <img src="images/fotos/<?php echo e($notificacion[0]->imagen); ?>" alt="Image" class="img-fluid">
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </main>
                <?php endif; ?>
            </section>
        </main>
        </div>
      </div>
      <?php echo $__env->make('general.Footer.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
  </main>

  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </body>
</html>
<?php /**PATH /home/vagrant/code/trollers/resources/views/general/Notificaciones/view.blade.php ENDPATH**/ ?>