
<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>

  <div class="site-wrap">
  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.usuarios.estilos', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


  <main class="main-content">

  <h2>Tabla de pagos</h2>
  <div class="table-wrapper">
    <table class="fl-table">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Estado</th>
            <th>Fecha</th>
            <?php if(Auth::user()->id==1): ?>
            <th>Acciones</th>
            <?php endif; ?>
        </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $usuarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td style="color:black;"><b><?php echo e($usuario->name); ?></b></td>
            <?php if($usuario->pago==1): ?>
                    <td style="color:green"><b>Pagado</b></td>
                    <?php else: ?>
                    <td style="color:red"><b>No Pagado</b></td>
                <?php endif; ?>   
            <td>
                <?php if($usuario->fecha_pago): ?>
                <b><?php echo e($usuario->fecha_pago); ?></b>
                <?php else: ?> 
                -
                <?php endif; ?>
            </td>
            <?php if(Auth::user()->id==1): ?>
            <td>
              <form action="<?php echo e(route('pagar')); ?>" method="post">
                  <?php echo csrf_field(); ?> 
                  <div class="form-group" hidden>
                      <input name="id" value="<?php echo e($usuario->id); ?>">
                  </div>
                  <div class="form-group">
                  <?php if(Auth::user()->id==1 && $usuario->pago==0): ?>
                      <input type="submit" style="border-radius: 10px; background-color: #83DFA4;" value="<?php echo e(trans('messages.2p')); ?>" name="submit"></div>
                  <?php endif; ?>
              </form>
              <form action="<?php echo e(route('renovar')); ?>" method="post">
                  <?php echo csrf_field(); ?> 
                  <div class="form-group" hidden>
                      <input name="id" value="<?php echo e($usuario->id); ?>">
                  </div>
                  <div class="form-group">
                  <?php if(Auth::user()->id==1 && $usuario->pago==1): ?>
                      <input type="submit" style="border-radius: 10px; background-color: #83D3DF;" value="<?php echo e(trans('messages.re')); ?>" name="submit"></div>
                  <?php endif; ?>
              </form>
            </td>
            <?php endif; ?>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <tbody>
    </table>
    <p style="color:white; margin-top:40px; text-align:center;"><a style="color:white;" href="/archivos/documentacion/facturas.png">Ver ficha de facturaciones del año 2021</a></p>
    <?php echo $__env->make('general.Footer.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </main>
  </body>
  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</html>
<?php /**PATH /home/vagrant/code/trollers/resources/views/general/usuarios/pagos.blade.php ENDPATH**/ ?>