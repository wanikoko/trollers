<div class="row justify-content-center">
    <div class="col-md-12 text-center py-5">
        <p><a href="https://www.instagram.com/trollers.es/">Trollers</a> &copy; 2013 - 2020</p>

        <!-- Idiomas -->
          
        <a href="<?php echo e(route('change_lang', ['lang' => 'es'])); ?>" class="bandera"><img src="https://image.flaticon.com/icons/png/512/206/206724.png" alt="Smiley face" height="32" width="32"></a>
        <a href="<?php echo e(route('change_lang', ['lang' => 'en'])); ?>" ><img src="https://image.flaticon.com/icons/png/512/206/206592.png" alt="Smiley face" height="32" width="32"></a>
        <br><br>

        <!-- Idiomas End -->
        <?php if(Auth::check()): ?>
        <p><a href="cuenta" style="color:white"><?php echo e(Auth::user()->name); ?></a></p>
        <a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <span style="color:white"><?php echo e(trans('messages.cs')); ?></span>
        </a>
        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
            <?php echo csrf_field(); ?>
        </form>
        <?php endif; ?>
    </div>
    </div>
</div><?php /**PATH /var/www/trollers/html/releases/33/resources/views/general/Footer/footer.blade.php ENDPATH**/ ?>