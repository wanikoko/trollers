<header class="header-bar d-flex d-lg-block align-items-center">
  <div class="site-logo">
    <a href="/">The Trollers 3.0</a>
  </div>

  <div class="d-inline-block d-xl-none ml-md-0 ml-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

  <div class="main-menu">
    <ul class="js-clone-nav">
      <li><a href="/"><?php echo e(trans('messages.home')); ?></a></li>
      <li><a href="/timeline">Timeline</a></li>
      <li><a href="/proyectos"><?php echo e(trans('messages.pr')); ?></a></li>
      <li><a href="/formacion"><?php echo e(trans('messages.for')); ?></a></li>
      <li><a href="/spotify">Spotify Wrapped</a></li>
      <li><a href="/boe">BOE</a></li>
      <!-- <li><a href="proyectos">Versiones</a></li> -->
      <?php if(Auth::check()): ?>
      <li><a href="/cuenta"><?php echo e(Auth::user()->name); ?></a></li>
      <li><a href="/protocolos"><?php echo e(trans('messages.pro')); ?></a></li>
      <?php if(Auth::user()->rol==1): ?>
      <li><a href="/imagen"><?php echo e(trans('messages.addimg')); ?></a></li>
      <li><a href="/eliminar"><?php echo e(trans('messages.delimg')); ?></a></li>
      <li><a href="/crear-boe"><?php echo e(trans('messages.adda')); ?></a></li>
      <li><a href="/proyecto"><?php echo e(trans('messages.addpr')); ?></a></li>
      <li><a href="/crear-protocolo"><?php echo e(trans('messages.addpro')); ?></a></li>
      <?php endif; ?>
      <li><a href="/pagos"><?php echo e(trans('messages.pay')); ?></a></li>
      <?php endif; ?>
      <?php if(!Auth::check()): ?>
      <br><br><br><br>
      <?php echo $__env->make('general.Login.login', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      <?php else: ?>
      <!-- <a href="/cuenta"><img src="../images/perfiles/<?php echo e(Auth::user()->imagen); ?>" style="border-radius: 50%;" height="40" width="40"></a> -->
      <li href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        <span style="color:white; cursor: pointer;"><?php echo e(trans('messages.cs')); ?></span>
      </li>
      <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
        <?php echo csrf_field(); ?>
      </form>
      <?php endif; ?>
    </ul>
    <ul class="social js-clone-nav">
      <li><a href="https://www.instagram.com/trollers.es/"><span class="icon-instagram"></span></a></li>
    </ul>
  </div>
</header><?php /**PATH /var/www/trollers/html/releases/33/resources/views/general/Header/header.blade.php ENDPATH**/ ?>