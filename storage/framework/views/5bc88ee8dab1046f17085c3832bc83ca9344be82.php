<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
  <div class="site-wrap">
  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.Notificaciones.estilos', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  
  <main class="main-content">
    <div class="container-fluid photos">
      <div class="row justify-content-center">
        <div class="col-md-6 pt-4">
        <div class="row pt-4 mb-3 text-center">
        </div>
          <a class='button' style="color: white;" href='nueva-notificacion'>
                Escribir
           </a><br><br>
        <main class="page">
            <section class="page__container">
            <h1>
            Notificaciones
              </h1><br>
                <p>
                  Total de notificaciones sin leer: <?php echo e($notificaciones->where("leido",false)->count()); ?>

                </p>
                <main>
                  <table>
                    <thead>
                      <tr>
                        <th>
                          Ministerio
                        </th>
                        <th>
                          Asunto
                        </th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $notificaciones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notificacion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <tr>
                        <td data-title='Ministerio'>
                            <?php if($notificacion->leido): ?>
                            <?php echo e(App\User::where("id",$notificacion->id_origen)->get()[0]->cargo); ?>

                            <?php else: ?>
                            <b><?php echo e(App\User::where("id",$notificacion->id_origen)->get()[0]->cargo); ?></b>
                            <?php endif; ?>
                        </td>
                        <td data-title='Asunto'>
                          <?php if($notificacion->leido): ?>
                              <?php echo e($notificacion->cabecera); ?>

                          <?php else: ?>
                              <b><?php echo e($notificacion->cabecera); ?></b>
                          <?php endif; ?>
                        </td>
                        <td class='select'>
                          <a class='button' style="color: white;" href='/ver-notificacion-<?php echo e($notificacion->id); ?>'>
                            Ver
                          </a>
                        </td>
                      </tr>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                  </table>
                </main>
        </main>
        <br>
        <?php if($recientes->count() > 0): ?>
        <main class="page">
            <section class="page__container">
            <h1>
            Enviadas Recientemente
              </h1><br>
                <main>
                  <table>
                    <thead>
                      <tr>
                        <th>
                          Destinatario
                        </th>
                        <th>
                          Asunto
                        </th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $recientes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reciente): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <tr>
                        <td data-title='Destino'>
                            <?php if($reciente->leido): ?>
                            <?php echo e(App\User::where("id",$reciente->id_destino)->get()[0]->cargo); ?>

                            <?php else: ?>
                            <b><?php echo e(App\User::where("id",$reciente->id_destino)->get()[0]->cargo); ?></b>
                            <?php endif; ?>
                        </td>
                        <td data-title='Asunto'>
                        <?php if($reciente->leido): ?>
                          <?php echo e($reciente->cabecera); ?>

                        <?php else: ?>
                          <b><?php echo e($reciente->cabecera); ?></b>
                        <?php endif; ?>
                        </td>
                        <td class='select'>
                          <a class='button' style="color: white;" href='/ver-notificacion-<?php echo e($reciente->id); ?>'>
                            Ver
                          </a>
                        </td>
                      </tr>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                  </table>
                </main>
        </main>
        <?php endif; ?>
        </div>
      </div>
      <?php echo $__env->make('general.Footer.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
  </main>

  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </body>
</html>
<?php /**PATH /Users/carlosrobles/Servidor/Trollers/resources/views/general/Notificaciones/index.blade.php ENDPATH**/ ?>