<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<body>
  <div class="site-wrap" >

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

    <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <main class="main-content">
      <div class="container-fluid photos">

        <div class="row pt-4 mb-5 text-center">
          <div class="col-12">
            <h2 class="text-white mb-4" id="header"><?php echo e(trans('messages.o')); ?></h2>
          </div>
        </div>

        <div class="row align-items-stretch">

          <?php $__currentLoopData = $imagenes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $imagen): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="col-6 col-md-6 col-lg-6 " data-aos="fade-up">
            <a href="images/fotos/<?php echo e($imagen->name); ?>" class="d-block photo-item" data-fancybox="gallery">
              <img src="images/fotos/<?php echo e($imagen->name); ?>" alt="Image" class="img-fluid">
              <div class="photo-text-more">
                <span class="icon icon-expand"></span>
              </div>
            </a>
            <?php if($edit): ?>
            <form action="<?php echo e(route('eliminar-privada')); ?>" method="post">
              <?php echo csrf_field(); ?>
              <div class="form-group" hidden>
                <input name="id" value="<?php echo e($imagen->id); ?>">
              </div>
              <div class="form-group" hidden>
                <input name="name" value="<?php echo e($imagen->name); ?>">
              </div>
              <div class="form-group">
                <input type="submit" style="margin-bottom:15px;" onclick="return confirm('¿Seguro que quieres eliminar esta foto privada?')" class="btn btn-danger" value="<?php echo e(trans('messages.del')); ?>" name="submit">
              </div>
            </form>
            <?php endif; ?>
          </div>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>

        <?php echo $__env->make('general.Footer.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

      </div>
    </main>

    <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</body>

</html>
<?php /**PATH /home/vagrant/code/trollers/resources/views/general/GaleriaPrivada/index.blade.php ENDPATH**/ ?>