<!DOCTYPE html>
<html lang="en">
  <?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
    
  <div class="site-wrap">

  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <main class="main-content">

  <div class="container-fluid photos">
        
        <div class="row pt-4 text-center">
          <div class="col-12">
            <h2 class="text-white mb-4" id="header">Perfil</h2>
          </div>
        </div>
        
      <div class="row align-items-stretch">
          <div class="col-9 col-md-9 col-lg-9" style="width:70%; margin:30px;" data-aos="fade-up">
            <?php if(count($usuario->images) > 0): ?>
              <a href="<?php echo e($usuario->images[0]->url); ?>" class="d-block photo-item" data-fancybox="gallery">
              <img src="<?php echo e($usuario->images[0]->url); ?>" alt="Image" class="img-fluid">
            <?php endif; ?>
              <h3 class="text-white">Usuario: <?php echo e($usuario->display_name); ?></h3>
              <?php if($usuario->product=='premium'): ?>
                <h3 style="color:green">Premium</h3>
              <?php else: ?>
                <h3  style="color:blue">No premium</h3>
              <?php endif; ?>
              <h3 class="text-white">Seguidores: <?php echo e($usuario->followers->total); ?></h3>
            </a>
          </div>     
      </div>

    <div class="container-fluid photos">
        
      <div class="row pt-4 mb-5 text-center">
        <div class="col-12">
          <h2 class="text-white mb-4" id="header">Tus Artistas Favoritos</h2>
        </div>
      </div>
      
	  <div class="row align-items-stretch">

      <?php $__currentLoopData = $artistas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $artista): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-6 col-md-6 col-lg-6" data-aos="fade-up">
          <a href="<?php echo e($artista->images[0]->url); ?>" class="d-block photo-item" data-fancybox="gallery">
            <img src="<?php echo e($artista->images[0]->url); ?>" alt="Image" class="img-fluid">
            <h3 class="text-white mb-4" style="text-align:center"><?php echo e($artista->name); ?></h3>
            <div class="photo-text-more">
              <span><h3 class="text-white mb-4"><?php echo e($artista->name); ?></h3></span>
            </div>
          </a>
        </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
        <?php if(count($artistas) == 0): ?>
            <h2 style="width:70%; margin:30px; color:red">No existen artistas favoritos, debes de escuchar más música</h2>
        <?php endif; ?>  
      </div>

    </div>

    <div class="container-fluid photos">
        
      <div class="row pt-4 mb-5 text-center">
        <div class="col-12">
          <h2 class="text-white mb-4" id="header">Tus Canciones Favoritas</h2>
        </div>
      </div>
      
	  <div class="row align-items-stretch">

      <?php $__currentLoopData = $temas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tema): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-6 col-md-6 col-lg-6" data-aos="fade-up">
          <a href="<?php echo e($tema->album->images[0]->url); ?>" class="d-block photo-item" data-fancybox="gallery">
            <img src="<?php echo e($tema->album->images[0]->url); ?>" alt="Image" class="img-fluid">
            <h3 class="text-white mb-4" style="text-align:center"><?php echo e($tema->name); ?></h3>
            <div class="photo-text-more">
              <span><h3 class="text-white mb-4"><?php echo e($tema->name); ?></h3></span>
            </div>
          </a>
        </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php if(count($temas) == 0): ?>
            <h2 style="width:70%; margin:30px; color:red">No existen caniones favoritas, debes de escuchar más música</h2>
        <?php endif; ?>
      </div>

      <?php echo $__env->make('general.Footer.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    </div>
  </main>

  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </body>
</html>
<?php /**PATH /var/www/trollers/html/releases/33/resources/views/general/Cuenta/music.blade.php ENDPATH**/ ?>