<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
  <div class="site-wrap">
  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  
  <main class="main-content">
    <div class="container-fluid photos">
      <div class="row justify-content-center">
        <div class="col-md-6 pt-4">
        <h4 class="text-white mb-4" data-aos="fade-up">Edición de articulo - Boletín Oficial del Estado</h4>
          <h4 class="text-white mb-4" data-aos="fade-up">Redacción del <?php echo e(Auth::user()->cargo); ?></h4>
          <h4 class="text-white mb-4" data-aos="fade-up">Articulo sobre <?php echo e($articulo->titulo); ?> del <?php echo e($articulo->fecha); ?> por el Excmo. Sr <?php echo e(Auth::user()->name); ?></h4>
          <form action="<?php echo e(route('editar-boe')); ?>" method="post">
            <?php echo csrf_field(); ?> 
                <div class="form-group">
                    <input name="titulo" style="background-color: white; color:black; border-radius: 10px; padding: 15px; width: 100%;" maxlength="180" value="<?php echo e($articulo->titulo); ?>">                
                </div>
                <div class="form-group">
                    <textarea id="textArea" class="form-control" name="descripcion" style="background-color: white; color:black; border-radius: 10px; padding: 15px;" rows="15" maxlength="4000"><?php echo e($articulo->descripcion); ?></textarea>
                </div>
                <div class="form-group" hidden>
                    <input name="id" value="<?php echo e($articulo->id); ?>">
                </div>
                <div class="form-group">
                    <input type="submit" style="border-radius: 10px; background-color: #FFC686;" value="Guardar" name="submit">
                    <a href="../../../boe" style="margin:10px;" class="btn btn-light"><?php echo e(trans('messages.volver')); ?></a>
                </div>
          </form>
        </div>
      </div>
    </div>
  </main>

  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </body>
</html>

<?php echo $__env->make('general.Editor.editor', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/trollers/resources/views/general/BOE/form-boe.blade.php ENDPATH**/ ?>