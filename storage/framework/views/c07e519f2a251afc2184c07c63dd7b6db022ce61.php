<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('general.Head.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <body>
  <div class="site-wrap">
  <div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>

  <?php echo $__env->make('general.Header.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  
  <main class="main-content">
    <div class="container-fluid photos">
      <div class="row justify-content-center">
        <div class="col-md-6 pt-4">
          <h2 class="text-white mb-4" data-aos="fade-up"><?php echo e(Auth::user()->name); ?></h2>
          <h4 class="text-white mb-4" data-aos="fade-up"><?php echo e(trans('messages.e_d')); ?></h4>
          <form action="<?php echo e(route('descripcion')); ?>" method="post">
            <?php echo csrf_field(); ?> 
                <div class="form-group">
                    <textarea class="form-control" name="texto" style="background-color: white; color:black; border-radius: 10px; padding: 15px;" rows="15" maxlength="9000"><?php echo e(Auth::user()->descripcion); ?></textarea>
                </div>
                <div class="form-group" hidden>
                    <input name="id" value="<?php echo e(Auth::user()->id); ?>">
                </div>
                <div class="form-group">
                    <input type="submit" style="border-radius: 10px; background-color: #FFC686;" value="<?php echo e(trans('messages.e_d2')); ?>" name="submit">
                    <a href="cuenta" style="margin:10px;" class="btn btn-light"><?php echo e(trans('messages.volver')); ?></a>
                </div>
          </form>
        </div>
      </div>
    </div>
  </main>

  <?php echo $__env->make('general.Links.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </body>
</html>
<?php /**PATH /var/www/trollers/html/releases/33/resources/views/general/Cuenta/editar.blade.php ENDPATH**/ ?>