<?php echo $__env->make('general.Home.visitors.estilos', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="container_visitors">
  <span>Visitantes por país en directo</span>
  <div class="places_container">
    <div class="places">
      <span><?php echo e(rand(1,15)); ?> desde Argentina 🇦🇷</span>
      <span><?php echo e(rand(1,15)); ?> desde Australia 🇦🇺</span>
      <span><?php echo e(rand(1,10)); ?> desde Austria 🇦🇹</span>
      <span><?php echo e(rand(1,10)); ?> desde Bélgica 🇧🇪</span>
      <span><?php echo e(rand(1,15)); ?> desde Brasil 🇧🇷</span>
      <span><?php echo e(rand(1,15)); ?> desde Canadá 🇨🇦</span>
      <span><?php echo e(rand(1,15)); ?> desde Colombia 🇨🇴</span>
      <span><?php echo e(rand(1,15)); ?> desde Costa Rica 🇨🇷</span>
      <span><?php echo e(rand(1,15)); ?> desde Dinamarca 🇩🇰</span>
      <span><?php echo e(rand(1,15)); ?> desde Ecuador 🇪🇨</span>
      <span><?php echo e(rand(1,15)); ?> desde Finlandia 🇫🇮</span>
      <span><?php echo e(rand(1,15)); ?> desde Francia 🇫🇷</span>
      <span><?php echo e(rand(1,100)); ?> desde EE.UU 🇺🇸</span>
      <span><?php echo e(rand(1,15)); ?> desde Alemania 🇩🇪</span>
      <span><?php echo e(rand(1,10)); ?> desde Islandia 🇮🇸</span>
      <span><?php echo e(rand(1,10)); ?> desde Irlanda 🇮🇪</span>
      <span><?php echo e(rand(1,10)); ?> desde Luxemburgo 🇱🇺</span>
      <span><?php echo e(rand(1,15)); ?> desde Malta 🇲🇹</span>
      <span><?php echo e(rand(1,15)); ?> desde Irlanda 🇳🇱</span>
      <span><?php echo e(rand(1,15)); ?> desde Nueva Zelanda🇳🇿</span>
      <span><?php echo e(rand(1,15)); ?> desde Noruega 🇳🇴</span>
      <span><?php echo e(rand(1,15)); ?> desde Portugal 🇵🇹</span>
      <span><?php echo e(rand(1,15)); ?> desde Sudáfrica 🇿🇦</span>
      <span><?php echo e(rand(1,100)); ?> desde España 🇪🇸</span>
      <span><?php echo e(rand(1,15)); ?> desde Suecia 🇸🇪</span>
      <span><?php echo e(rand(1,15)); ?> desde Taiwan 🇹🇼</span>
      <span><?php echo e(rand(1,25)); ?> desde Reino Unido 🇬🇧</span>
      <span><?php echo e(rand(1,15)); ?> desde Uruguay 🇺🇾</span>
    </div>
  </div>
</div>
<?php /**PATH /home/vagrant/code/trollers/resources/views/general/Home/visitors/index.blade.php ENDPATH**/ ?>