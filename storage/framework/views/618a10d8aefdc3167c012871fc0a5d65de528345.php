<?php echo $__env->make('general.Home.info.estilos', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="table center">
  <div class="monitor-wrapper center">
    <div class="monitor center">
      <?php if($alerta->nivel==3): ?>
      <a style="color: red;"><?php echo $alerta->texto ?></a>
      <?php elseif($alerta->nivel==2): ?>
      <a style="color: #AD45FF"><?php echo $alerta->texto ?></a>
      <?php elseif($alerta->nivel==1): ?>
      <a style="color: #454DFF"><?php echo $alerta->texto ?></a>
      <?php endif; ?>
    </div>
  </div>
</div>


<?php /**PATH /home/vagrant/code/trollers/resources/views/general/Home/info/index.blade.php ENDPATH**/ ?>