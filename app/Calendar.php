<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $table = 'calendar';

    protected $fillable = [
        'id',
        'titulo',
        'descripcion',
        'fecha',
        'finalDate',
        'tipo',
        'persona',
        'image'
    ];
}
