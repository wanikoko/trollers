<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@show')->name('home');

Route::get('/proyectos', 'ProyectoController@show')->name('proyectos');
Route::get("/eliminar-proyecto", "ProyectoController@show");
Route::post("/eliminar-proyecto", "ProyectoController@destroy")->name('eliminar-proyecto');

Route::get('/formacion', 'UserController@show')->name('formacion');
Route::get('/enigma', 'EnigmaController@index')->name('enigma');
Route::get('/manager', 'EnigmaController@manager')->middleware('auth')->middleware('admin')->name('manager');
Route::post("/manager-generate", "EnigmaController@manager_generate")->name('manager-generate');
Route::post("/codificar", "EnigmaController@code")->name('codificar');
Route::post("/decodificar", "EnigmaController@decode")->name('decodificar');


Route::get('/boe', 'BOEcontroller@index')->name('boe');
Route::get("/crear-boe", "BOEcontroller@create")->middleware('auth')->middleware('admin');
Route::post("/crear-boe", "BOEcontroller@store")->name('crear-boe');
Route::get("/editar-boe/{id}", "BOEcontroller@edit")->middleware('auth')->middleware('admin');
Route::post("/editar-boe", "BOEcontroller@update")->name('editar-boe');
Route::get("/eliminar-boe", "BOEcontroller@index");
Route::post("/eliminar-boe", "BOEcontroller@destroy")->name('eliminar-boe');

Route::get('/minecraft', 'MinecraftController@index')->name('minecraft');
// Route::get('/add-minecraft', 'MinecraftController@create');
// Route::post("/add-minecraft", "MinecraftController@store")->name('add-minecraft');

Route::get('/mw3', 'MW3Controller@index')->name('mw3');
Route::get('/crear-mw3', 'MW3Controller@create');
Route::post("/crear-mw3", "MW3Controller@store")->name('crear-mw3');

Route::get('/calendario', 'UserController@calendar')->name('calendar');
Route::get("/crear-evento", "UserController@addEvent")->middleware('auth')->middleware('admin');
Route::post("/crear-evento", "UserController@createEvent")->name('crear-evento');
Route::post("/eliminar-evento", "UserController@deleteEvent")->name('eliminar-evento');


Route::get('/alerta', 'ControllerAlertas@index')->middleware('auth')->middleware('admin')->name('alerta');
Route::post("/activacion-alerta", "ControllerAlertas@edit")->name('activacion-alerta');
Route::post("/crear-alerta", "ControllerAlertas@store")->name('crear-alerta');

Route::get('/netflix', 'StoriesController@index')->middleware('auth')->middleware('admin');
Route::get('/certificado-covid', 'UserController@certificado')->middleware('auth')->middleware('admin');

Route::get("/galeria-privada", "GaleriaController@index")->middleware('auth')->middleware('admin');
Route::get("/galeria-privada/{id}", "GaleriaController@index")->middleware('auth')->middleware('admin');
Route::post("/eliminar-privada", 'GaleriaController@destroy')->name('eliminar-privada');

Route::get('/terminal', 'TerminalController@index')->name('terminal')->middleware('auth');
Route::get('/protocolos', 'ProtocoloController@index')->name('protocolos')->middleware('auth');
Route::get("/crear-protocolo", "ProtocoloController@create")->middleware('auth')->middleware('admin');
Route::post("/crear-protocolo", "ProtocoloController@store")->name('crear-protocolo');
Route::get("/editar-protocolo/{id}", "ProtocoloController@edit")->middleware('auth')->middleware('admin');
Route::post("/editar-protocolo", "ProtocoloController@update")->name('editar-protocolo');
Route::get("/eliminar-protocolo", "ProtocoloController@index");
Route::post("/eliminar-protocolo", "ProtocoloController@destroy")->name('eliminar-protocolo');

Route::get('/cuenta', 'UserController@index')->middleware('auth');
Route::get('/notificaciones', 'NotificationsController@index')->middleware('auth')->name("notificaciones");
Route::get('/ver-notificacion-{id}', 'NotificationsController@show')->middleware('auth');
Route::get('/nueva-notificacion', 'NotificationsController@create')->middleware('auth');
Route::post('/nueva-notificacion', 'NotificationsController@save')->middleware('auth')->name("nueva-notificacion");
Route::get('/responder-notificacion-{id}', 'NotificationsController@responder')->middleware('auth');
Route::post('/responder-notificacion', 'NotificationsController@save')->middleware('auth')->name("responder-notificacion");

Route::get('/descripcion', 'UserController@edit')->middleware('auth');
Route::post("/descripcion", "UserController@update")->name('descripcion');

Route::get('/password', 'UserController@passEdit')->middleware('auth');
Route::post("/password", "UserController@passUpdate")->name('password');

Route::get('/proyecto', 'ProyectoController@create')->middleware('auth')->middleware('admin');
Route::post("/proyecto", "ProyectoController@store")->name('proyecto');

Route::get('/imagen', 'ImagenController@create')->middleware('auth')->middleware('admin');
Route::post("/imagen", "ImagenController@store")->name('imagen');

Route::get('/editar', 'ImagenController@edit')->middleware('auth');
Route::post("/editar", "ImagenController@update")->name('editar');

Route::get('/eliminar', 'HomeController@destroy')->middleware('auth')->middleware('admin');
Route::post("/eliminar", 'ImagenController@destroy')->name('eliminar');

Route::get('/pagos', 'PagosController@index')->middleware('auth')->name('pagos');;
Route::post("/proceso", "PagosController@update")->name('pagar');
Route::get('/proceso', 'HomeController@show');
Route::post("/renovar", "PagosController@renovar")->name('renovar');
Route::get('/renovar', 'HomeController@show');

Route::get('/spotify', 'SpotifyController@index');
Route::get('/spotify-biblioteca', 'Biblioteca@index');
Route::get('/timeline', 'HomeController@timeline');

Route::get('/callback', 'SpotifyController@create');
Route::get('/callback_2', 'Biblioteca@create');

Route::get('lang/{lang}', function ($lang) {
  Session::put('lang', $lang);
  return Redirect::back();
})->middleware('web')->name('change_lang');
